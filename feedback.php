<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="feedback/feedbackstyle.css" />
   

    <title>Feedback</title>
  </head>
  <body>
     

<div class="content">
	<div class="contact">
		<div class="other">
			<div class="info">
				<h3>Email</h3>
				<div class="svg-wrap">
					<a href="mailto:connor@connorgaunt.com"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 485.211 485.211"><path d="M485.21 363.906c0 10.637-2.99 20.498-7.784 29.174l-153.2-171.41 151.54-132.584c5.894 9.355 9.445 20.344 9.445 32.22v242.6zM242.607 252.793l210.863-184.5c-8.654-4.737-18.398-7.642-28.91-7.642H60.65c-10.523 0-20.27 2.906-28.888 7.643l210.844 184.5zm58.787-11.162l-48.81 42.735c-2.854 2.487-6.41 3.73-9.977 3.73-3.57 0-7.125-1.243-9.98-3.73l-48.82-42.736-155.14 173.6c9.3 5.834 20.198 9.33 31.984 9.33h363.91c11.785 0 22.688-3.496 31.984-9.33l-155.15-173.6zM9.448 89.086C3.554 98.44 0 109.43 0 121.305v242.602c0 10.637 2.978 20.498 7.79 29.174L160.97 221.64 9.448 89.086z"/></svg>
					<a href="12210050@gmail.com">12210050@gmail.com</a></a>
				</div>
				<h3>Contact</h3>
                <p style="color: darkorange;"><a href="17719585">17719585</a></p>
			</div>
		</div>
		<div class="form">
			<h1>FEEDBACK</h1>
			<?php
			if(isset($_GET['status'])){
				echo "<p class='text' style='color:blue; font-size:small; height:3px; text-align:center;'>";
				switch($_GET['status']){
					case 'emailsend':
						echo 'Your feedback has been submitted';
				}
				echo "</p>";
			}
			?>
			<form action="feedback.inc.php" method="post">
				<div class="flex-rev">
					<input type="text" placeholder="Name" name="name" id="name" title="Name required" required/>
					<label for="name" >Name</label>
				</div>
				<div class="flex-rev">
					<input type="email" placeholder="12210050@gmail.com" name="email" id="email" title="Email Required" required/>
					<label for="email">Your Email</label>
				</div>
				<div class="flex-rev">
					<textarea placeholder="Your Feedback" name="message" id="message" requ></textarea>
					<label for="message">Your Feedback</label>
				</div>
				
				 <div class="d-grid gap-2 d-md-block">
					<!-- <button type="button"><a href="index.html" style="text-decoration:none; color: white">Back</a></button> -->
					<button type="reset" class="resetbtn">Back</button>
					<button type="submit" name="sendbutton">Send</button>
				</div>
			</form>
		</div>
	</div>
</div>
  </body>
  </html>
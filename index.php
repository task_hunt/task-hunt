<?php include('server.php') ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!-- animation link -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
  <link rel="stylesheet" href="homestyle.css" />

  <!-- BoxIcon -->
  <link href="https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css" rel="stylesheet" />

  <title>Task Hunt</title>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Task Hunt</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" href="index.php">HOME</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Gallery
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li>
                <a class="dropdown-item" href="jobgallery.php">Job Gallery</a>
              </li>
              <li>
                <a class="dropdown-item" href="taskersgallery.php">Taskers Gallery</a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="about.php">ABOUT</a>
          </li>
        </ul>
        <!-- <form class="d-flex">
            <input
              class="form-control me-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            /> -->
        <a href="search.php"> <button class="btn btn-outline-success" type="submit">Search</button></a>
        <!-- </form> -->


        <svg xmlns="http://www.w3.org/2000/svg" width="45" height="50" fill="currentColor" class="bi bi-person-circle p-2" viewBox="0 0 16 16" style="color: white" data-bs-toggle="modal" data-bs-target="#Login" style="cursor: pointer">
          <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
          <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
        </svg>


      </div>
    </div>
  </nav>
  <div>
    <?php if (isset($_SESSION['success'])) : ?>
      <div class="error success">
        <h3>
          <?php
          echo $_SESSION['success'];
          unset($_SESSION['success']);
          ?>
        </h3>
      </div>
    <?php endif ?>
    <?php if (isset($_SESSION['email'])) : ?>
      <p> <strong><?php echo $_SESSION['email']; ?></strong></p>
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php endif ?>
  </div>

  <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" style="height: 50%">
    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="home/home2 (1).jpg" class="d-block w-100" alt="..." style="height: 600px" />
        <div class="carousel-caption d-none d-md-block">
          <h5>We work to earn our leisure.</h5>
          <strong>Aristotle</strong>
          <div>
            <button type="button" style="height: 0.9cm; width: 3cm" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#signin">
              join us
            </button>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <img src="home/home1.jpg" class="d-block w-100" alt="..." style="height: 600px" />
        <div class="carousel-caption d-none d-md-block">
          <h5>We work to earn our leisure.</h5>
          <strong>Aristotle</strong>
          <div>
            <button type="button" style="height: 0.9cm; width: 3cm" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#signin">
              join us
            </button>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <img src="home/home3.jpg" class="d-block w-100" alt="..." style="height: 600px" />
        <div class="carousel-caption d-none d-md-block">
          <h5>We work to earn our leisure.</h5>
          <strong>Aristotle</strong>
          <div>
            <button type="button" style="height: 0.9cm; width: 3cm" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#signin">
              join us
            </button>
          </div>
        </div>
      </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>

  <div class="row py-5">
    <div class="col-lg-6 m-auto text-center">
      <h1 class="animate__animated animate__heartBeat animate__infinite infinite" style="font-family: 'Lobster', cursive">
        <div class="animate__animated animate__bounce animate__faster">
          Taskers Around You
        </div>
      </h1>
      <h5 class="" style="color: black; font-family: cursive">
        Get the best service
      </h5>
    </div>
  </div>




  <section class="sec bg-light justify-content-center align-items-center h-100 min-vh-100">
    <!-- <div class="container"> -->
    <div class="row gy-4">


      <?php
      $conn = mysqli_connect("localhost", "root", "", "taskhunt");
      $query = "SELECT *from taskers";
      $query_run =  mysqli_query($conn, $query);

      $check_tasker = mysqli_num_rows($query_run) > 0;

      if ($check_tasker) {
        while ($row = mysqli_fetch_array($query_run)) {

      ?>


          <div class="col-lg-3 col-md-4">
            <div class="card bg-light h-100 d-flex p-2 flex-column text-dark ">
              <img width="280px" height="200px" src="upload/<?php echo $row['image'] ?>" alt="image" />
              <div class="description">
                <P><strong>Name:<?php echo $row['name']; ?></strong></P>
                <P><strong>Contact:<?php echo $row['contact']; ?></strong></P>
                <P><strong>Email:<?php echo $row['email']; ?></strong></P>
                <P><strong>SKill:<?php echo $row['skill']; ?></strong></P>
                <!-- <P><strong>Description:<?php echo $row['description']; ?></strong></P> -->

                <button type="text" style="
                        border: none;
                        background-color: rgba(255, 255, 255, 0.267);
                        " data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
                  <strong>More..</strong>
                </button>

              </div>
            </div>
          </div>

          <!-- offcanva -->
          <!-- <div class="offcanvas offcanvas-start" style="margin-bottom: 1cm; margin-top: 8px" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
                        <div class="offcanvas-header">
                            <h5 class="offcanvas-title" id="offcanvasExampleLabel">
                                Offcanvas
                            </h5>
                            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                        <div class="offcanvas-body">
                            <div>
                                <img width="280px" height="200px" src="upload/<?php echo $row['image'] ?>" alt="image" />
                            </div>
                            <div>
                                <P><strong>Name:<?php echo $row['name']; ?></strong></P>
                                <P><strong>Contact:<?php echo $row['contact']; ?></strong></P>
                                <P><strong>Email:<?php echo $row['email']; ?></strong></P>
                                <P><strong>SKill:<?php echo $row['skill']; ?></strong></P>
                                <P><strong>Description:<?php echo $row['description']; ?></strong></P>
                            </div>
                        </div>
                    </div> -->
          <!-- offcanva end -->


      <?php
        }
      } else {
        echo "No record found";
      }
      ?>

    </div>


    <!-- </div> -->
    <div class="d-grid  col-2 mx-auto">

      <a href="taskersgallery.php"> <button class="btn btn-info " type="button" style="text-align: center; margin-top: 0.5cm;width: fit-content;background-color: rgba(240, 248, 255, 0.904);">View All Tasker</button></a>
    </div>

    <!-- <div class="card0 text-center" style="margin-left: 4cm;margin-right: 4cm;margin-top: 1cm; background-color: rgb(169, 133, 204);border-radius: 30px;">
            <div class="card-body">
                <h5 class="card-title">Aim high, Work hard and Love your family.</h5>
                <strong class="card-text">-Deborah Roberts</strong>
                <br>
                <a href="tasker.html" class="btn btn-primary">Join As Tasker</a>
            </div> -->
    </div>
    <div class="row py-5 " style="margin-top: 3cm;">
      <div class="col-lg-6 m-auto text-center">
        <h1 class="animate__animated animate__heartBeat animate__infinite infinite" style="font-family: 'Lobster', cursive">
          <div class="animate__animated animate__bounce animate__faster">
            Jobs Around You
          </div>
        </h1>
        <h5 class="" style="color: black; font-family: cursive">
          Get jobs with a click
        </h5>
      </div>
    </div>
  </section>
  <section class="sec bg-light justify-content-center align-items-center h-100 min-vh-100">
    <!-- <div class="container"> -->
    <div class="row gy-2">

      <?php
      $conn = mysqli_connect("localhost", "root", "", "taskhunt");
      $query = "SELECT *from jobs";
      $query_run =  mysqli_query($conn, $query);

      $check_jobs = mysqli_num_rows($query_run) > 0;

      if ($check_jobs) {
        while ($row = mysqli_fetch_array($query_run)) {

      ?>


          <div class="col-lg-3 col-md-4">
            <div class="card bg-light h-100 d-flex p-2 flex-column text-dark ">
              <img width="280px" height="200px" src="jobupload/<?php echo $row['Image'] ?>" alt="image" />
              <div class="description">
                <P><strong>Job:<?php echo $row['Job']; ?></strong></P>
                <P><strong>Location:<?php echo $row['Location']; ?></strong></P>
                <P><strong>Email:<?php echo $row['Email']; ?></strong></P>
                <P><strong>Contact:<?php echo $row['Contact']; ?></strong></P>

                <!-- <button type="text" style="
                        border: none;
                        background-color: rgba(255, 255, 255, 0.267);
                        " data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
                                    <strong>More..</strong>
                                </button> -->

              </div>
            </div>
          </div>
      <?php
        }
      } else {
        echo "No record found";
      }
      ?>

    </div>
    <!-- </div> -->
    <div class="d-grid  col-2 mx-auto">
      <a href="jobgallery.php"><button class="btn btn-info" type="button" style="text-align: center; margin-top: 0.5cm;width: fit-content;background-color: rgba(240, 248, 255, 0.904);">View All Jobs</button></a>
    </div>
    <br>

  </section>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <!-- sign up modal -->
  <div class="modal fade" id="signin" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <!-- signup Form -->
        <form action="index.php" method="POST">
          <?php include('error.php'); ?>
          <div class="modal-header" style="background-color: #0dcaf0">
            <h5 class="modal-title" style="padding-left: 5.5cm; color: rgb(2, 6, 10)">
              Signup
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="mb-3">
              <label for="Username">Username<span class="text-danger">*</span></label>
              <input type="text" name="username" class="form-control" id="Username" placeholder="Enter Username" title="Username required" value="<?php echo $username; ?>" required />
            </div>
            <div class="mb-3">
              <label for="Username">Email<span class="text-danger">*</span></label>
              <input type="email" name="email" class="form-control" id="email" placeholder="Enter your email" title="email required" value="<?php echo $email; ?>" required />
            </div>

            <div class="mb-3">
              <label for="Password">Password<span class="text-danger">*</span></label>
              <input type="password" name="password" class="form-control" id="Password" placeholder="Enter Password" title="Password Required" required />
            </div>
            <div class="mb-3">
              <input class="form-check-input" type="checkbox" value="" id="remember" />
              <label class="form-check-label" for="remember">Remember Me</label>
            </div>
          </div>
          <div class="modal-footer pt-4">
            <a href="user.html" style="text-decoration: none; position: stretch">
              <button type="submit" class="btn btn-success" name="signup_btn">Sign up</button>
            </a>
          </div>
          <!-- <p class="text-center">
              Have Account? <a href="#" style="color: black">login</a>
            </p> -->
        </form>
      </div>
    </div>
  </div>

  <!-- modal-2 for log In icon -->
  <div class="modal fade" id="Login" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <!-- Login Form -->
        <form action="validatelogin.php" method="POST">
          <?php include('error.php'); ?>
          <div class="modal-header" style="background-color: #0dcaf0">
            <h5 class="modal-title" style="padding-left: 5.5cm; color: rgb(2, 6, 10)">
              Login
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="mb-3">
              <label for="Username">Username<span class="text-danger">*</span></label>
              <input type="text" name="username" class="form-control" id="username" placeholder="Enter Username" title="Username required" required />
            </div>

            <div class="mb-3">
              <label for="Password">Password<span class="text-danger">*</span></label>
              <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password" title="Password Required" required />
            </div>
            <div class="mb-3">
              <input class="form-check-input" type="checkbox" value="" id="remember" />

              <a href="ForgotPw.html" class="float-end" style="color: black">Forgot Password</a>
            </div>
          </div>
          <div class="modal-footer pt-4">
            <a href="user.html" style="text-decoration: none; position: stretch">
              <button type="submit" name="submit" class="btn btn-success mx-auto w-100">
                Login
              </button>
            </a>
          </div>
          <!-- <p class="text-center">New Here?</p> -->
        </form>
      </div>
    </div>
  </div>

  <br /><br /><br />
  <footer style="background-color: rgba(0, 0, 0, 0.856)">
    <div class="article">
      <div class="left box">
        <div class="upper">
          <div class="topic">TASK HUNT</div>
          <p>
            FreeHunt is a website developed by GCIT first year students as a
            group project under guidence of the lecturers. The key point of
            this website is to create a place to look for workers and jobs
            (for both long-term and short-term).
          </p>
        </div>
        <div class="lower"></div>
      </div>
      <div class="middle box">
        <div class="topic">Contact us</div>
        <div class="phone">
          <a href="#"><i class="fas fa-phone-volume"></i>+975 77422544</a>
        </div>
        <div class="email">
          <a href="#"><i class="fas fa-envelope"></i>Taskhunt@gmail.com</a>
        </div>
        <div>
          <a href="feedback.php">feedback</a>
        </div>
      </div>
    </div>
  </footer>
</body>

</html>
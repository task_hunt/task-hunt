<?php include('server.php') ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!-- animation link -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
  <!-- <link rel="stylesheet" href="homestyle.css" /> -->
  <!-- BoxIcon -->
  <link href="https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="search.css">

  <title>Task Hunt</title>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Task Hunt</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" href="index.php">HOME</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Gallery
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li>
                <a class="dropdown-item" href="jobgalery.php">Job Gallery</a>
              </li>
              <li>
                <a class="dropdown-item" href="taskersgalery.php">Taskers Gallery</a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="about.php">ABOUT</a>
          </li>
        </ul>

      </div>
    </div>
  </nav>
  <br>
  <br><br>
  <form method="post">
    <label></label>
    <input type="text" name="search">
    <input type="submit" name="submit">

  </form>



  <?php
  $con = new PDO("mysql:host=localhost;dbname=taskhunt", 'root', '');
  if (isset($_POST["submit"])) {
    $str = $_POST["search"];


    $query = "SELECT * FROM `taskers` WHERE skill = '$str'";
    $results = mysqli_query($db, $query);
    $userdata = $results->fetch_assoc();
    // echo $userdata['name'];
    if (mysqli_num_rows($results) > 0) {

  ?>
      <br>
      <div class="col-lg-3 col-md-4">
        <div class="card bg-light h-100 d-flex p-2 flex-column text-dark ">
          <img width="280px" height="200px" src="upload/<?php echo $userdata['image'] ?>" alt="image" />
          <div class="description">
            <P><strong>Name:<?php echo $userdata['name']; ?></strong></P>
            <P><strong>Contact:<?php echo $userdata['contact']; ?></strong></P>
            <P><strong>Email:<?php echo $userdata['email']; ?></strong></P>
            <P><strong>SKill:<?php echo $userdata['skill']; ?></strong></P>
            <!-- <P><strong>Description:<?php echo $userdata['description']; ?></strong></P> -->

          </div>
        </div>
      </div>

  <?php
    } else {
      echo "Name Does not exist";
    }
  }


  ?>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  <br><br><br>
  <footer style="background-color:rgba(0, 0, 0, 0.856) ;">
    <div class="article">
      <div class="left box">
        <div class="upper">
          <div class="topic">TASK HUNT</div>
          <p>FreeHunt is a website developed by GCIT first year students as a group project under guidence of the lecturers. The key point of this website is to create a place to look for workers and jobs (for both long-term and short-term).</p>
        </div>
        <div class="lower">

        </div>
      </div>
      <div class="middle box">
        <div class="topic">Contact us</div>
        <div class="phone">
          <a href="#"><i class="fas fa-phone-volume"></i>+975 77422544</a>
        </div>
        <div class="email">
          <a href="#"><i class="fas fa-envelope"></i>Taskhunt@gmail.com</a>
        </div>
        <div>
          <a href="feedback.php">feedback</a>
        </div>
      </div>

    </div>

  </footer>
</body>

</html>
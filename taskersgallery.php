<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- animation link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <link rel="stylesheet" href="homestyle.css" />
    <link rel="stylesheet" href="tasker.html">
    <!-- BoxIcon -->
    <link href="https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css" rel="stylesheet" />

    <title>Task Hunt</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Task Hunt</a>
            <!-- <img src="logo3.png" alt="" style="display: flex; height: 1.3cm;"> -->
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" href="home.php">HOME</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Gallery
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="jobgallery.php">Job Gallery</a></li>
                            <li><a class="dropdown-item" href="taskersgallery.php">Taskers Gallery</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="about.php">ABOUT</a>
                    </li>
                </ul>
                <!-- <form class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search"> -->
                <a href="search.php"> <button class="btn btn-outline-success" type="submit">Search</button></a>
                <!-- </form> -->
                <svg xmlns="http://www.w3.org/2000/svg" width="45" height="50" fill="currentColor" class="bi bi-person-circle p-2" viewBox="0 0 16 16" style="color: white" data-bs-toggle="modal" data-bs-target="#Login" style="cursor: pointer">
                    <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                    <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                </svg>
            </div>
        </div>
    </nav>
    <br>
    <br>
    <br>
    <br>
    <section class="sec bg-light justify-content-center align-items-center h-100 min-vh-100">
        <!-- <div class="container"> -->
        <div class="row gy-4">


            <?php
            $conn = mysqli_connect("localhost", "root", "", "taskhunt");
            $query = "SELECT *from taskers";
            $query_run =  mysqli_query($conn, $query);

            $check_tasker = mysqli_num_rows($query_run) > 0;

            if ($check_tasker) {
                while ($row = mysqli_fetch_array($query_run)) {

            ?>


                    <div class="col-lg-3 col-md-4">
                        <div class="card bg-light h-100 d-flex p-2 flex-column text-dark ">
                            <img width="280px" height="200px" src="upload/<?php echo $row['image'] ?>" alt="image" />
                            <div class="description">
                                <P><strong>Name:<?php echo $row['name']; ?></strong></P>
                                <P><strong>Contact:<?php echo $row['contact']; ?></strong></P>
                                <P><strong>Email:<?php echo $row['email']; ?></strong></P>
                                <P><strong>SKill:<?php echo $row['skill']; ?></strong></P>
                            </div>
                        </div>
                    </div>
            <?php
                }
            } else {
                echo "No record found";
            }
            ?>

        </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <br><br><br>
    <footer style="background-color:rgba(0, 0, 0, 0.856) ;">
        <div class="article">
            <div class="left box">
                <div class="upper">
                    <div class="topic">TASK HUNT</div>
                    <p>FreeHunt is a website developed by GCIT first year students as a group project under guidence of the lecturers. The key point of this website is to create a place to look for workers and jobs (for both long-term and short-term).</p>
                </div>
                <div class="lower">

                </div>
            </div>
            <div class="middle box">
                <div class="topic">Contact us</div>
                <div class="phone">
                    <a href="#"><i class="fas fa-phone-volume"></i>+975 77422544</a>
                </div>
                <div class="email">
                    <a href="#"><i class="fas fa-envelope"></i>Taskhunt@gmail.com</a>
                </div>
                <div>
                    <a href="feedback.php">feedback</a>
                </div>
            </div>

        </div>

    </footer>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
  <!-- BoxIcon -->
  <link href="https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css" rel="stylesheet" />
  <title>TaskHunt</title>
  <link rel="stylesheet" href="about.css" />
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Task Hunt</a>
      <!-- <img src="logo3.png" alt="" style="display: flex; height: 1.3cm;"> -->
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" href="home.php">HOME</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Gallery
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li>
                <a class="dropdown-item" href="jobgallery.php">Job Gallery</a>
              </li>
              <li>
                <a class="dropdown-item" href="taskersgallery">Taskers Gallery</a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="about.php">ABOUT</a>
          </li>
        </ul>
        <!-- <form class="d-flex">
            <input
              class="form-control me-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            /> -->
        <!-- <button class="btn btn-outline-success" type="submit">Search</button> -->
        </form>
        <svg xmlns="http://www.w3.org/2000/svg" width="45" height="50" fill="currentColor" class="bi bi-person-circle p-2" viewBox="0 0 16 16" style="color: white" data-bs-toggle="modal" data-bs-target="#Login" style="cursor: pointer">
          <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
          <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
        </svg>
      </div>
    </div>
  </nav>
  <br />

  <div class="container mt-5">
    <br />
    <!-- <h1 class="text-center">About Us</h1> -->

    <div class="row">
      <div class="vision">
        <h1>Vision</h1>
        <p>
          "Providing and allowing all sort of workers to deploy talennts; so
          <br />
          everyone gets employed and satisfied and that every customer <br />
          who leaves our web does so with a smile."
        </p>
      </div>
    </div>

    <h2 class="text-center">Our team</h2>
    <div class="row row-expand-lg justify-content-center">
      <div class="col col-lg-2 col-md-6 col-sm-12">
        <div class="card">
          <img src="about/puru.jpeg" alt="" class="card-img rounded-circle" />
          <h5>Puru Shotam</h5>
          <h6>Team Leader</h6>
        </div>
      </div>
      <div class="col col-lg-2 col-md-6 col-sm-12">
        <div class="card">
          <img src="about/sherub.jpeg" alt="photo" class="card-img rounded-circle" />
          <h5>Sherub Tenzing</h5>
          <h6>UI Design</h6>
        </div>
      </div>
      <div class="col col-lg-2 col-md-6 col-sm-12">
        <div class="card">
          <img src="about/yonten.jpeg" alt="" class="card-img rounded-circle" />
          <h5>Yonten</h5>
          <h6>Front-End Developer</h6>
        </div>
      </div>
      <div class="col col-lg-2 col-md-6 col-sm-12">
        <div class="card">
          <img src="about/tadin.jpeg" alt="" class="card-img rounded-circle" />
          <h5>Tandin Zangmo</h5>
          <h6>Analyst</h6>
        </div>
      </div>
      <div class="col col-lg-2 col-md-6 col-sm-12">
        <div class="card">
          <img src="about/dorji.jpg" alt="" class="card-img rounded-circle" />
          <h5>Dorji Phuntsho</h5>
          <h6>Back-End Developer</h6>
        </div>
      </div>
    </div>
  </div>

  <footer style="background-color: rgba(0, 0, 0, 0.856)">
    <div class="article">
      <div class="left box">
        <div class="upper">
          <div class="topic">TASK HUNT</div>
          <p>
            FreeHunt is a website developed by GCIT first year students as a
            group project under guidence of the lecturers. The key point of
            this website is to create a place to look for workers and jobs
            (for both long-term and short-term).
          </p>
        </div>
        <div class="lower"></div>
      </div>
      <div class="middle box">
        <div class="topic">Contact us</div>
        <div class="phone">
          <a href="#"><i class="fas fa-phone-volume"></i>+975 77422544</a>
        </div>
        <div class="email">
          <a href="#"><i class="fas fa-envelope"></i>Taskhunt@gmail.com</a>
        </div>
        <div>
          <a href="feedback.php">feedback</a>
        </div>
      </div>
    </div>
  </footer>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  <div class="modal fade" id="Login" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <!-- Login Form -->
        <form action="">
          <div class="modal-header">
            <h5 class="modal-title" style="padding-left: 5.5cm; color: rgb(2, 6, 10)">
              Login
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="mb-3">
              <label for="Username">Username<span class="text-danger">*</span></label>
              <input type="text" name="username" class="form-control" id="Username" placeholder="Enter Username" title="Username required" required />
            </div>

            <div class="mb-3">
              <label for="Password">Password<span class="text-danger">*</span></label>
              <input type="password" name="password" class="form-control" id="Password" placeholder="Enter Password" title="Password Required" required />
            </div>
            <div class="mb-3">
              <input class="form-check-input" type="checkbox" value="" id="remember" />
              <label class="form-check-label" for="remember">Remember Me</label>
              <a href="ForgotPw.html" class="float-end" style="color: black">Forgot Password</a>
            </div>
          </div>
          <div class="modal-footer pt-4">
            <button type="submit" class="btn btn-success mx-auto w-100">
              Login
            </button>
          </div>
          <p class="text-center">
            New Here? <a href="#" style="color: black">Signup</a>
          </p>
        </form>
      </div>
    </div>
  </div>
</body>

</html>
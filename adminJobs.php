<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <!-- box icon link -->
    <link href="https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="gallery.html">
    <link rel="stylesheet" href="view.html">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="dashboard.css">
    <!-- <title>Admin Dashboard</title> -->
</head>

<body>
    <div class="d-flex" id="wrapper">
        <!---Sidebar starts--->
        <div class="bg-white" id="sidebar-wrapper">
            <div class="sidebarbar-heading text-center py-4 primary-text fs-4 fw-bold text-uppercase border-bottom">
                TASK HUNT
            </div>

            <div class="list-group list-group-flush my-0">
                <a href="adminmain.php" class="side-nav">
                    <i class="bx bxs-store bx-tada"></i>Taskers
                </a>
                <a href="#" class="side-nav">
                    <i class="bx bx-signal-5 bx-flashing"></i>Jobs
                </a>

                <a href="Admin_feedback.php" class="side-nav">

                    <i class='bx bxs-comment-detail bx-tada'></i>Feedback
                </a>
                <a href="#" class="side-nav">
                    <i class='bx bx-log-out'></i>Logout
                </a>
            </div>
        </div>
        <!---Sidebar starts--->

        <div id="content">
            <?php
            $conn = mysqli_connect("localhost", "root", "", "taskhunt");
            $query = "SELECT *from jobs";
            $query_run =  mysqli_query($conn, $query);
            ?>

            <table class="table bg-white">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>job</th>
                        <th>location</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Image</th>
                        <th>DELETE</th>



                    <tr>
                </thead>
                <tbody>
                    <?php
                    // checks record
                    if (mysqli_num_rows($query_run) > 0) {
                        foreach ($query_run as $row) {
                    ?>
                            <tr>
                                <td> <?php echo $row['Id']; ?></td>
                                <td> <?php echo $row['Job']; ?></td>
                                <td> <?php echo $row['Location']; ?></td>
                                <td> <?php echo $row['Email']; ?></td>
                                <td> <?php echo $row['Contact']; ?></td>



                                <td>
                                    <img src="<?php echo "jobupload/" . $row['Image']; ?>" width="70px" height="70px" alt="image">
                                </td>
                                <td>
                                    <!-- <a href="" class="btn btn-danger">DELETE</a> -->
                                    <form action="code.php" method="POST">
                                        <input type="hidden" name="Deleteid" value="<?php echo $row['Id']; ?>">
                                        <input type="hidden" name="Delimage" value="<?php echo $row['Image']; ?>">
                                        <button type="submit" name="Delete_image" class="btn btn-danger">DELETE</button>
                                    </form>
                                </td>
                                <!-- <td> 
                                            <a href="" class="btn btn-danger">DELETE</a>

                                        </td> -->
                            </tr>
                        <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td>No record avaliable</td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>



        </div>
    </div>
    </div>
    </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta/dist/js/bootstrap.bundle.min.js"></script>

    </script>
</body>

</html>
<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <link rel="stylesheet" href="formstyle.css" />
    <!-- <link rel="stylesheet" href="index.html" /> -->

    <title>Post you Job</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Task Hunt</a>
            <!-- <img src="logo3.png" alt="" style="display: flex; height: 1.3cm;"> -->
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" href="home.php">HOME</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Gallery
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="jobgallery.php">Job Gallery</a></li>
                            <li><a class="dropdown-item" href="taskersgallery.php">Taskers Gallery</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="about.php">ABOUT</a>
                    </li>
                </ul>
                <!-- <form class="d-flex"> -->
                <!-- <input class="px-2 search text-dark" type="search" placeholder="Search" aria-label="Search" /> -->
                <!-- <a href="search.php"><button class="btn0" type="submit">Search</button></a>  -->
                <!-- </form> -->
                <!-- <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-person-circle p-2" viewBox="0 0 16 16" style="color: white" data-bs-toggle="modal" data-bs-target="#Login" style="cursor: pointer">
                    <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                    <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                </svg> -->
            </div>
        </div>
    </nav>
    <br />
    <br />
    <br />

    <section class="main">
        <div class="container ">
            <div class="row d-flex justify-content-center align-items-center ">
                <div class="col-xl-7">
                    <h1 class="text-dark mt-4" style="text-align: center">
                        Post your Job
                    </h1>


                    <?php
                    if (isset($_SESSION['status']) && $_SESSION != '') {
                    ?>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Hey!</strong> <?php echo $_SESSION['status']; ?>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>

                    <?php
                        unset($_SESSION['status']);
                    }
                    ?>

                    <form action="code1.php" method="POST" enctype="multipart/form-data">

                        <div class="card" style="border-radius: 15px">
                            <div class="card-body">
                                <div class="row align-items-center pt-4 pb-3">
                                    <div class="col-md-3 ps-5">
                                        <h6 class="mb-0">Job</h6>
                                    </div>
                                    <div class="col-md-9 pe-5">
                                        <input type="text" name="Job" class="form-control form-control-lg" placeholder="Job type" Required />
                                    </div>
                                </div>

                                <hr class="mx-n3" />

                                <div class="row align-items-center py-3">
                                    <div class="col-md-3 ps-5">
                                        <h6 class="mb-0">location</h6>
                                    </div>
                                    <div class="col-md-9 pe-5">
                                        <input type="text" name="Location" class=" form-control form-control-lg" placeholder="Location/place" Required />
                                    </div>
                                </div>

                                <hr class="mx-n3" />
                                <div class="row align-items-center pt-4 pb-3">
                                    <div class="col-md-3 ps-5">
                                        <h6 class="mb-0">Email</h6>
                                    </div>
                                    <div class="col-md-9 pe-5">
                                        <input type="email" name="Email" class="form-control form-control-lg" placeholder="example1233@gmail.com" Required />
                                    </div>
                                </div>

                                <hr class="mx-n3" />
                                <div class="row align-items-center pt-4 pb-3">
                                    <div class="col-md-3 ps-5">
                                        <h6 class="mb-0">Contact</h6>
                                    </div>
                                    <div class="col-md-9 pe-5">
                                        <input type="number hidden" name="Contact" maxlength="8" class=" form-control form-control-lg" placeholder="17/77" Required />
                                    </div>
                                </div>

                                <hr class="mx-n3" />

                                <div class="row align-items-center py-3">
                                    <div class="col-md-3 ps-5">
                                        <h6 class="mb-0">Upload Photo</h6>
                                    </div>
                                    <div class="col-md-9 pe-5">
                                        <input class="form-control form-control-lg" id="formFileLg" type="file" name="Image" />
                                    </div>
                                </div>

                                <hr class="mx-n3" />
                                </button>
                                <a href="home.php"><button type="submit" class="btn btn-primary" name="submit">
                                        Post Now
                                    </button></a>

                                <!-- <button type="submit" name="submit" class="btn btn-primary btn-lg" data-bs-toggle="modal" data-bs-target="#apply">
                  Post Now
                </button> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- model -->
        <!-- <div class="modal fade" id="apply" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 style="color: black" class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form style="color: black" action="#">
              <div class="mb-3">Are you sure?</div>
              <div class="mb-3"></div>
              <div class="mb-3"></div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
              Cancle
            </button>
            <a href="index.html"><button type="submit" class="btn btn-primary" name="submit">
                Submit
              </button></a>
          </div>
        </div>
      </div>
    </div> -->
    </section>

    <br><br><br>
    <footer style="background-color:rgba(0, 0, 0, 0.856) ;">
        <div class="article">
            <div class="left box">
                <div class="upper">
                    <div class="topic">TASK HUNT</div>
                    <p>FreeHunt is a website developed by GCIT first year students as a group project under guidence of the lecturers. The key point of this website is to create a place to look for workers and jobs (for both long-term and short-term).</p>
                </div>
                <div class="lower">

                </div>
            </div>
            <div class="middle box">
                <div class="topic">Contact us</div>
                <div class="phone">
                    <a href="#"><i class="fas fa-phone-volume"></i>+975 77422544</a>
                </div>
                <div class="email">
                    <a href="#"><i class="fas fa-envelope"></i>Taskhunt@gmail.com</a>
                </div>
                <div>
                    <a href="feedback.php">feedback</a>
                </div>
            </div>

        </div>

    </footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>
<?php
session_start();
$conn = mysqli_connect("localhost", "root", "", "taskhunt");

// for taskers
if (isset($_POST['submit'])) {
    $name = $_POST['name'];
    $contact = $_POST['contact'];
    $email = $_POST['email'];
    $skill = $_POST['skill'];
    $image = $_FILES['image']['name'];

    // check image extensions
    $allowed_extension = array('gif', 'png', 'jpg', 'jpeg');
    $filename = $_FILES['image']['name'];
    $file_extension = pathinfo($filename, PATHINFO_EXTENSION);

    if (!in_array($file_extension, $allowed_extension)) {
        $_SESSION['status'] = "image should be in jpg ,png, jpeg and gif";
        header('location:taskerform.php');
    } else {



        // check images there or not
        if (file_exists("upload/" . $_FILES['image']['name'])) {
            $filename = $_FILES['image']['name'];
            $_SESSION['status'] = "You have already uploaded";
            header('location:taskerform.php');
        } else {
            $query = "INSERT INTO taskers (name, contact, email, skill,image) 
    VALUES ('$name', '$contact','$email','$skill','$image')";
            $query_run = mysqli_query($conn, $query);

            if ($query_run) {

                move_uploaded_file($_FILES["image"]["tmp_name"], "upload/" . $_FILES["image"]["name"]);
                $_SESSION['status'] = "upload  successfull";
                header('location:home.php');
            } else {

                $_SESSION['status'] = "upload not sucessful";
                header('location:taskerform.php');
            }
        }
    }
}

if (isset($_POST['delete_image'])) {
    $id = $_POST['deleteid'];
    $delimage = $_POST['delimage'];

    $query = "DELETE FROM taskers WHERE id='$id' ";
    $query_run = mysqli_query($conn, $query);

    if ($query_run) {
        unlink("upload/" . $delimage);
        $_SESSION['status'] = "Delete sucessful";
        header('location:adminmain.php');
    } else {
        $_SESSION['status'] = "Delete not sucessful";
        header('location:adminmain.php');
    }
}

// LOGIN USER
if (isset($_POST['login_user'])) {
    $username = mysqli_real_escape_string($db, $_POST['username']);
    $password = mysqli_real_escape_string($db, $_POST['password']);

    if (empty($username)) {
        array_push($errors, "User Name is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }

    // ADMIN LOGIN
    if ($username == 'admin' && $password == '123456') {
        $_SESSION['username'] = $username;
        $_SESSION['success'] = "You are now logged in";
        header('location: adminmain.php');
    }

    if (count($errors) == 0) {
        $password = md5($password);
        $query = "SELECT * FROM users WHERE username='$username' AND password='$password'";
        $results = mysqli_query($db, $query);
        if (mysqli_num_rows($results) == 1) {
            $_SESSION['username'] = $username;
            $_SESSION['success'] = "You are now logged in";
            header('location: user.php');
        } else {
            array_push($errors, "Wrong username/password combination");
        }
    }
}
